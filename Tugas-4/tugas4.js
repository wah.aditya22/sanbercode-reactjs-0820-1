//soal 1
var hitung = 1;
while (hitung != 0) {
	while (hitung <= 20) {
		if (hitung == 1){
			console.log("looping pertama");
		}
		if (hitung % 2 == 0){
			console.log(hitung + " - I love Coding");
		}
		hitung++;
	}
	while (hitung > 0){
		if (hitung == 21){
			console.log(" Looping Kedua ");
		}
		if (hitung % 2 == 0){
			console.log(hitung + " - I Will become a fronted developer");
		}
		hitung--;
	}
}


//soal 2
for (var i = 1; i < 21; i++){
	if (i == 1 || i % 2 !=0){
		if(i % 3 == 0){
			console.log(i + " - i Love Coding");
		}else{
			console.log(i+ " - Santai");
		}
	}
	if(i % 2 == 0){
		console.log(i + " - Berkualitas");
	}
}

//soal3
var hastag;
for (hastag = 1; hastag < 9 ; hastag ++ ){
	var pagar = Array(hastag).join("#");
	console.log(pagar)
}

//soal4
var kalimat = "saya sangat senang belajar javascript"
var jadiarray = kalimat.split(" ")
console.log(jadiarray)

//soal5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var	daftarBuahUrut = daftarBuah.sort();
for (var i = 0; i < daftarBuahUrut.length; i++){
	console.log(daftarBuahUrut[i]);
} 