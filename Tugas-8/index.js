//soal1
var readBooks = require("./callback.js")

var books = [
{name: 'LOTR', timeSpent: 3000},
{name: 'Fidas', timeSpent: 2000},
{name: 'Kalkulus', timeSpent: 4000},
{name: 'Komik', timeSpent:1000},
]

let i = 1;

function antrianBuku(waktu, book){
	readBooks(waktu, book, function(waktu){
		if(i<=3){
			antrianBuku(waktu, books[i]);
			i++;
		}
	})
}

antrianBuku(10000,books[0])