var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 

let i = 1;

function antrianBuku(waktu, book){
	if (i<3){
		readBooksPromise(waktu,book)
		.then(function(waktu){
			antrianBuku(waktu, books[i])
			i++;
		})
		.catch(function(error){
			console.log(error.message);
		})
	}
}

antrianBuku(10000 ,books[0])