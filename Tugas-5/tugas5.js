//soal 1

function halo(){
	return " Halo Sanbers! "
}

console.log(halo())

//soal 2

function kalikan ( num1, num2 ) {
	return num1*num2
}

var num1 = 12
var num2 = 4

var hasilkali = kalikan(num1, num2)
console.log(hasilkali)

//soal 3

function introduce(name, age, address, hobby){
	return " nama saya " + name + "," + " umur saya " + age + " tahun " + " , " + " alamat saya di " + address + " , " + " saya punya hobby " + hobby
} 

var name = " John "
var age = 30
var address = " Jalan Belum Jadi "
var hobby = " Gaming "

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

//soal 4

var arraydaftarpeserta = [" Asep ", " Laki - Laki ", " Baca Buku ", 1992 ]
var objekdaftarpeserta = { nama: arraydaftarpeserta[0], jeniskelamin: arraydaftarpeserta[1], hobbyy: arraydaftarpeserta[2], tahunlahir: arraydaftarpeserta[3] }
console.log(objekdaftarpeserta)

//soal 5

var arrobj = [{
	nama : " strawberry ",
	warna : " merah ",
	adabiji : "tidak",
	harga : 9000,
},
{
	nama : " jeruk ",
	warna : " oranye ",
	adabiji : "ada",
	harga : 8000,
},
{
	nama : " semangka ",
	warna : " hijau & merah ",
	adabiji : " ada ",
	harga : 10000,
},
{
	nama : " pisang ",
	warna : " kuning ",
	adabiji : "tidak",
	harga : 5000,
}]
console.log(arrobj[0])

//soal 6
var datafilm =[" Harry Potter ", 125, " Horror", 2000]

function tambahdatafilm(arr){
	var obj = new Object();
	obj.name = arr[0];
	obj.durasi = arr[1];
	obj.genre = arr[2];
	obj.tahun = arr[3]
	return obj
}
console.log(tambahdatafilm(datafilm))